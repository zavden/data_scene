# DATA SCENE

The first thing to do before reading the data is to see the discrete distance of time, that is:
```
0,-0.229843318462
0.05,-0.0766144394875
0.1,-0.258573770523
0.15,-0.124498426914
0.2,-0.325611352921
0.25,-0.416591048241
```
We can see that the time advances every 0.05 seconds, so that will be our value `"dt_data"` in the CONFIG dict.

It is also necessary to have an idea of what the maximum and minimum values are, and enter them in the global variables (see image)
![ShowDataExample](ShowDataExample.png)

## CONFIG Dictionary explanation.

* `include_grid`: To include grid (`True` by default)
* `grid_config`: Grid config
* `velocity_time`: 1 by default
* `initial_shift`: Value at the center of the graph at the beginning of the animation
* `total_time`: Final value you want to reach
* `data_file`: Full data path
* `x_max_label`: It is the largest X-axis label, only valid if `x_max_label > velocity_time`
* `include_updaters`: Include updaters
* `remove_first_labels`: If you use `"initial_shift": 0`, you will notice that there are negative labels on the timeline, if you want to remove them, include how many of these you want to remove. That is, if your timeline is "-2 -1 0 1 2]", then use `"remove_first_labels" :2` (remove the -2 and -1).
* `x_label_position_at_y`: Y coordinate (with respect to the graph, not the screen) of the X axis labels
* The `x_initial_config` and `y_initial_config` are similar to `GraphScene`