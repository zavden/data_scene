from manimlib.imports import *
from active_projects.data_scene.imports import *

try:
    set_custom_quality(500,7)
except:
    pass
# python3 -m manim active_projects\data_scene\test.py ShowData -pk
Y_MAX = 0.1
Y_MIN = -0.6
DT = 0.1
Y_SIZE = 6
Y_TICK_FREQUENCY = 0.1 
class ShowData(DataGraphScene):
    CONFIG = {
        #GRID CONFIG
        "include_grid": True,
        "grid_config": {},
        #TIME CONFIG
        "velocity_time": 1,
        "initial_shift": 5,
        "total_time": 25,
        #DATA CONFIG
        "data_file": "active_projects/data_scene/data2",
        "dt_data":0.05,
        #GRAPH CONFIG
        "x_max_label": 30, # <- total_time + initial_shift < x_max_label
        "graph_origin": [-5, 2, 0],
        "include_updaters": True,
        "remove_first_labels": 3,
        "x_label_position_at_y": 0.1,
        "x_initial_config": {
            "direction": "X", # <- ALWAYS PUT IT
            #LABEL
            "axis_label": "$t$",
            "label_position": [6,2,0], # <- If None then use default, Try [-2,2,0]
            #SIZE GRAPH
            "v_max": 8,
            "v_min": 0, # <- No modify
            "axis_size": 10,
            "tick_frequency": 1, #<- Steps of graph (dt)
            "decimal_config": {
                "num_decimal_places":1
            },
        },
        "y_initial_config": {
            "direction": "Y", # <- ALWAYS PUT IT
            #LABEL
            "axis_label": "$m/s^2$",
            "label_position": [-4,-3,0], # <- If None then use default, Try [2,-2,0]
            #SIZE GRAPH
            "v_max": Y_MAX,
            "v_min": Y_MIN,
            "axis_size": Y_SIZE,
            "tick_frequency": Y_TICK_FREQUENCY,
            "leftmost_tick": None, # <- leftmost_tick must be < v_min
            "decimal_config": {
                "unit": "",
                "num_decimal_places":2
            },
            "labeled_nums": list(np.arange(
                                            float(Y_MIN), # <- v_min
                                            float(Y_MAX) + float(DT), # <- v_max + dt
                                            float(DT) # dt
                )
            )
        },

    }
    def construct(self):
        self.setup_axes(False)
        self.x_max = self.x_initial_config["v_max"]
        self.x_min = self.x_initial_config["v_min"]
        self.y_max = self.y_initial_config["v_max"]
        self.y_min = self.y_initial_config["v_min"]
        x_min_coord = self.coords_to_point(self.x_min,0)[0]
        x_max_coord = self.coords_to_point(self.x_max,0)[0]
        self.x_axis_lenght = self.x_max - self.x_min
        self.desf = int(abs(self.x_max - self.x_min) / 2)
        self.shift_origin = ValueTracker(self.initial_shift)
        self.shift_y = ValueTracker(0)
        shift_origin = self.shift_origin
        #self.x_base_axis[2].fade(1)
        self.data_fixed = get_coords_from_csv_fixed(
            self.data_file,
            pre_seconds=self.desf,
            dt=self.dt_data
        )
        self.graph_initial_group = self.get_graph_group(self.x_min,self.x_max,self.include_updaters)
        # CHECK THE MAX LABEL NUMBER
        x_max_shift_graph = self.initial_shift + self.total_time
        if self.x_max_label != None:
            if self.x_max_label < x_max_shift_graph:
                x_numberline_max = x_max_shift_graph
            else:
                x_numberline_max = self.x_max_label
        else:
            x_numberline_max = x_max_shift_graph

        unit_size = self.x_initial_config["axis_size"] / self.x_axis_lenght
        self.x_initial_numberline = self.get_x_numberline(
            x_max_label = self.x_max,
            x_max=x_numberline_max + 1,
            numberline_config = {
                "unit_size": unit_size, 
                "tick_frequency": self.x_initial_config["tick_frequency"],
                "numbers_to_show": list(
                                        np.arange(
                                            -self.desf,
                                            self.x_max_label + self.x_initial_config["tick_frequency"],
                                            self.x_initial_config["tick_frequency"]
                                        )
                                    ),
                "decimal_number_config": self.x_initial_config["decimal_config"]
            },
            updaters = self.include_updaters,
            y_label_position=self.x_label_position_at_y
        )
        """
        self.y_initial_config["labeled_nums"] = list(np.arange(
            self.y_initial_config["v_min"],
            self.y_initial_config["v_max"] + self.y_initial_config["tick_frequency"],
            self.y_initial_config["tick_frequency"]
            )
        )
        self.y_initial_numberline = self.get_y_numberline(
            self.y_initial_config,
            self.include_updaters
        )
        """

        self.add(
            self.x_initial_numberline,
            #self.y_initial_numberline,
            self.graph_initial_group,
        )
        self.wait()
        self.shift_graph()
        self.wait()


    def shift_graph(self):
        self.play(
                self.shift_origin.set_value,self.initial_shift + self.total_time,
                #self.shift_y.set_value,-4,
                rate_func=linear,
                run_time=self.total_time*self.velocity_time,
            )

    def get_x_numberline(self,x_max_label,x_max,d_fade=1,y_label_position=-0.3,numberline_config={},updaters=True):
        if "unit_size" in numberline_config:
            unit_size = numberline_config["unit_size"]
        else:
            unit_size = 1
        desf = int(abs(x_max_label/2))
        numberline = NumberTextLine(
            x_min=-desf,
            x_max=x_max,
            include_numbers=True,
            **numberline_config
        )
        numberline[0].fade(1)
        #print(len(numberline[3]))
        #numberline[3][:desf].fade(1)
        def x_numberline_update(d_fade=d_fade,y_label_position=y_label_position):
            end_i   = self.coords_to_point(self.x_min,0)[0]
            start_i = self.coords_to_point(self.x_min + d_fade,0)[0]
            end_f   = self.coords_to_point(self.x_max - d_fade,0)[0]
            start_f = self.coords_to_point(self.x_max,0)[0]
            def numberline_update(mob):
                mob.next_to(self.graph_origin + LEFT*self.shift_origin.get_value()*unit_size,RIGHT,buff=0)
                line,thicks,other,labels = mob
                initial_width_fix = labels[0].get_width()/2
                mob.shift(LEFT*initial_width_fix)
                labels.set_y(self.coords_to_point(0,y_label_position)[1])
                thicks.set_y(self.x_base_axis[1].get_y())
                fade_objects = [*labels,*thicks]
                for label in fade_objects:
                    coord_x = label.get_x()
                    if end_f <= coord_x <= start_f:
                        alpha = abs( (start_f - coord_x) / (end_f - start_f) )
                        fade = interpolate(0,1,alpha)
                        label.set_fill(opacity=fade)
                        label.set_stroke(opacity=fade)
                    elif start_i < coord_x < end_f:
                        label.set_fill(opacity=1)
                        label.set_stroke(opacity=1)
                    elif end_i <= coord_x <= start_i:
                        alpha = abs( (start_i - coord_x) / (end_i - start_i) ) 
                        fade = interpolate(0,1,alpha)
                        label.set_fill(opacity=1-fade)
                        label.set_stroke(opacity=1-fade)
                    else:
                        label.set_fill(opacity=0)
                        label.set_stroke(opacity=0)
                    if self.remove_first_labels != 0:
                        for mob in numberline[3][:self.remove_first_labels+1]:
                            mob.set_fill(opacity=0)
                return mob
            return numberline_update
        x_numberline_update_start = x_numberline_update()
        x_numberline_update_start(numberline)
        if updaters:
            numberline.add_updater(x_numberline_update_start)
        return numberline

    def get_y_numberline(self,y_config,updaters=True):
        def get_numerline(v_min,
                         v_max,
                         axis_size, #axis_width
                         direction="Y",
                         labeled_nums=None,
                         leftmost_tick=None,
                         tick_frequency=1,
                         label_position=None,
                         decimal_config={},
                         tick_size=0.1,
                         label_scale=0.4,
                         line_to_number_buff=0.2,
                         exclude_zero_label=True,
                         axis_label=None):
            num_range = float(v_max - v_min)
            space_unit_to = axis_size / num_range
            print(space_unit_to)
            if labeled_nums is None:
                labeled_nums = []
            if leftmost_tick is None:
                leftmost_tick = v_min
            if direction == "Y":
                label_direction = LEFT
            else:
                label_direction = None
            axis = NumberTextLine(
                x_min=v_min,
                x_max=v_max,
                unit_size=space_unit_to,
                tick_frequency=tick_frequency,
                leftmost_tick=leftmost_tick,
                numbers_with_elongated_ticks=labeled_nums,
                line_to_number_buff=line_to_number_buff,
                color=self.axes_color,
                decimal_number_config=decimal_config,
                number_scale_val=label_scale,
                label_direction=label_direction,
            )
            axis.ticks = axis[2]
            axis.shift(self.graph_origin - axis.number_to_point(0))
            if direction == "Y":
                axis.rotate(np.pi / 2, about_point=axis.number_to_point(0))
            if len(labeled_nums) > 0:
                if exclude_zero_label:
                    labeled_nums = [x for x in labeled_nums if x != 0]
                axis.add_numbers(*labeled_nums)
            labels = axis[3]
            y_numberline = VGroup(labels,axis.ticks)
            return y_numberline
        y_numberline = get_numerline(**y_config)

        def y_numberline_update(d_fade=0.1,x_buff=-1):
            end_i   = self.coords_to_point(0,self.y_min - d_fade)[1]
            start_i = self.coords_to_point(0,self.y_min)[1]
            end_f   = self.coords_to_point(0,self.y_max)[1]
            start_f = self.coords_to_point(0,self.y_max + d_fade)[1]
            def numberline_update(mob):
                mob.next_to(self.graph_origin,LEFT,buff=0)
                mob.shift(DOWN*self.shift_y.get_value())
                labels,thicks = mob
                initial_width_fix = thicks[0].get_width()/2
                mob.shift(RIGHT*initial_width_fix)
                #print(mob.get_center())
                thicks.set_x(self.y_base_axis[1].get_x())
                for label in [*labels,*thicks]:
                    coord_y = label.get_y()
                    if end_f <= coord_y <= start_f:
                        alpha = abs( (start_f - coord_y) / (end_f - start_f) )
                        fade = interpolate(0,1,alpha)
                        label.set_fill(opacity=fade)
                        label.set_stroke(opacity=fade)
                    elif start_i < coord_y < end_f:
                        label.set_fill(opacity=1)
                        label.set_stroke(opacity=1)
                    elif end_i <= coord_y <= start_i:
                        alpha = abs( (start_i - coord_y) / (end_i - start_i) ) 
                        fade = interpolate(0,1,alpha)
                        label.set_fill(opacity=1-fade)
                        label.set_stroke(opacity=1-fade)
                    else:
                        label.set_fill(opacity=0)
                        label.set_stroke(opacity=0)
                return mob
            return numberline_update

        y_numberline_update()(y_numberline)
        if updaters:
            y_numberline.add_updater(y_numberline_update())
        return y_numberline



    def get_graph_group(self,x_min,x_max,updaters=True):
        graph_group = VGroup(VMobject(),Dot())
        def get_graph_group_update(x_min,x_max):
            def graph_group_update(mob):
                p,d = mob
                graph_from_data = self.get_graph_from_data(
                        self.data_fixed,
                        x_min,
                        x_max,
                        self.shift_origin.get_value()-self.desf,
                )
                p.become(
                    self.fade_graph_extremes(graph_from_data,0.2)
                )
                d.move_to(graph_from_data.point_from_proportion(0.5))
            return graph_group_update

        get_graph_group_update(x_min,x_max)(graph_group)
        if updaters:
            graph_group.add_updater(get_graph_group_update(x_min,x_max))
        return graph_group


class TestFade(Scene):
    def construct(self):
        numberline = NumberTextLine(
                x_min=-4,
                x_max=4,
                include_numbers=True
            )
        line,thicks,other,labels = numberline
        line.set_color(RED)
        thicks.set_color(ORANGE)
        thicks[3].set_color(TEAL)
        labels.set_color(PINK)
        other.set_color(BLUE)
        numberline.next_to(ORIGIN,RIGHT,buff=0)
        shift = ValueTracker(0)

        start = -1
        end = -2
        def numberline_update(mob):
            mob.next_to(ORIGIN + [shift.get_value(),0,0],RIGHT,buff=0)
            line,thicks,other,labels = mob
            print(*[thicks[i].get_x() for i in range(len(thicks))])
            for i in range(len(thicks)):
                coord_x = thicks[i].get_x()
                if start < coord_x:
                    thicks[i].set_fill(opacity=1)
                if end <= coord_x <= start:
                    alpha = abs( (start - coord_x) / (end - start) ) 
                    fade = interpolate(0,1,alpha)
                    thicks[i].set_fill(opacity=1-fade)
                if coord_x < end:
                    thicks[i].set_fill(opacity=0)


        numberline_update(numberline)
        numberline.add_updater(numberline_update)
        self.add(numberline)
        #"""
        self.play(
            shift.set_value,-7,
            rate_func=linear,
            run_time=5
        )
        #"""
        self.wait()

        #print(self.x_axis_lenght)
        #print(self.x_initial_config["axis_size"])
        #print(unit_size)
        """
        new_unit_size = 0.5
        x_new_numberline = self.get_x_numberline(
            x_max_label = self.x_max, 
            x_max= x_numberline_max + 1,
            numberline_config = {"unit_size":  0.5}
        )
        """

        # GET Y NUMBERLINE

        """
        y_new_numberline = self.get_y_numberline(
            {
                "v_min": -1,
                "v_max": 1,
                "axis_size": 12,
                "tick_frequency": 0.2,
                "leftmost_tick": None,
                "labeled_nums": list(np.arange(-1,1+0.2,0.2)),
                "direction": "Y",
                "tick_size": 0,
                "decimal_config": {
                    "unit": "V",
                    "num_decimal_places":1
                },
                "label_scale": 0.4,
            }
        )
        """